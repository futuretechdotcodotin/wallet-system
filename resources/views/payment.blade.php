@extends('layouts.app')
@section('content')
<div class="container">  
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <div class="row text-center">
                        <h3 class="card-heading">Payment Details</h3>
                    </div>                    
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{ session('success') }}</p>
                        </div>
                    @endif
                    <!-- @if (Session::has('success'))
                        <div class="alert alert-success text-center">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <p>{{-- Session::get('success') --}}</p>
                            {{session()->get('success')}}
                        </div>
                    @endif -->

                    <form role="form" action="{{ route('stripe.payment') }}" id="payment-form" method="post" class="validation" data-cc-on-file="false" data-stripe-publishable-key="pk_test_51GQUdMEGSkW7kzz6ay98mLSiV69wAcV2KU288Ic0cbVG9Ko6l8xYzcSggeEi9tHJ353AITkXFcmGuHGlO84kxgDF002Hh5Yngd" id="payment-form">
                        @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id }}">
                            <p class="error"></p>
                            <div class='form-group row required'>
                                <label class='col-md-4 col-form-label text-md-right'>Name on Card</label> 
                                <div class="col-md-6">
                                    <input class='form-control' size='4' name="card_name" type='text'>   
                                </div>
                            </div>
                            <div class='form-group row required'>
                                <label class='col-md-4 col-form-label text-md-right'>Add Money</label> 
                                <div class="col-md-6">
                                    <input class='form-control' size='4' name="money" type='text'>   
                                </div>
                            </div>
  
                        
                            <!-- <div class='form-group row form-group card required'>
                                <label class='col-md-4 col-form-label text-md-right'>Card Number</label> 
                                <div class="col-md-6">
                                    <input autocomplete='off' class='form-control card-num' size='20'
                                    type='text'>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Card number') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control card-num" name="card_number" required autocomplete="off">

                                </div>
                            </div>

                            <div class='form-group row form-group cvc required'>
                                <label class='col-md-4 col-form-label text-md-right'>CVC</label> 
                                <div class="col-md-6">
                                    <input autocomplete='off' class='form-control card-cvc' placeholder='e.g 415' size='4'
                                    type='password' name="cvc">
                                </div>
                            </div>
                            <div class='form-group row expiration required'>
                                <label class='col-md-4 col-form-label text-md-right'>Expiration Month</label> <div class="col-md-6">
                                    <input class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='number' name="month">
                                </div>
                            </div>
                            <div class='form-group row expiration required'>
                                <label class='col-md-4 col-form-label text-md-right'>Expiration Year</label> 
                                <div class="col-md-6">
                                <input class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='number'  name="year">
                                </div>
                            </div>
                        </div>
  
                        <div>
                            <div class='col-md-12 hide error error-custom form-group' style="display: none;">
                                <div class='alert-danger alert'></div>
                            </div>
                        </div>
  <!-- 
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-danger btn-lg btn-block" type="submit">Pay Now\</button>
                            </div>
                        </div> -->
                        <div class="card-footer">
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                     <button class="btn btn-danger btn-sm" type="submit">Pay Now</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</div>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form         = $(".validation");
  $('form.validation').bind('submit', function(e) {
    var $form       = $(".validation"),
        inputVal    = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputVal),
        $errorStatus = $form.find('div.error'),
        valid         = true;
        $errorStatus.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorStatus.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.attr('data-stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-num').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeHandleResponse);
    }
  
  });
  
  function stripeHandleResponse(status, response) {
        if (response.error) {
            $('.error').show()
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            var token = response['id'];
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});
</script>
@endsection