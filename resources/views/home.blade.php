@extends('layouts.app')

@section('content')
    
<input type="hidden" id="userId" value="{{ Auth::user()->id}}">
<div class="container">
     @if ($message = Session::get('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ $message }}</p>
            </div>
        @endif
    <div class="row justify-content-center">

        <div class="col-md-3">
            <div class="card">
                <div class="card-header">{{ __('Product 1') }}</div>
                <div class="card-body">
                    <img src="{{ asset('images/wallet.png') }}">
                    <p class="product-price" price="1">Price : 1 coins</p>   
                </div>
                <div class="card-footer">
                    <button class="btn btn-success buy">Buy</button>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">{{ __('Product 2') }}</div>
                <div class="card-body">
                    <img src="{{ asset('images/wallet.png') }}">
                    <p class="product-price" price="1">Price : 1 coins</p>   
                </div>
                <div class="card-footer">
                    <button class="btn btn-success buy">Buy</button>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">  
                <div class="card-header">{{ __('Product 3') }}</div>
                <div class="card-body">
                    <img src="{{ asset('images/wallet.png') }}">
                    <p class="product-price" price="3">Price : 3 coins</p>   
                </div>
                <div class="card-footer">
                    <button class="btn btn-success buy">Buy</button>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">{{ __('Product 4') }}</div>
                <div class="card-body">
                    <img src="{{ asset('images/wallet.png') }}">
                    <p class="product-price" price="4">Price : 4 coins</p>   
                </div>
                <div class="card-footer">
                    <button class="btn btn-success buy">Buy</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
