@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 mx-auto">
			<div class="card">
				<div class="card-header">
					<h3 class="d-inline">Transaction History</h3>
					<a href="{{ url('/add-points')}}" class="btn btn-link float-right">Add Points</a>
				</div>
				<div class="card-body">
					<table class="table table-hover table-bordered">
							<thead class="table-dark">
								<tr>
									<th>#</th>
									<th> {{	__('Coins') }}</th>
									<th> {{	__('Payment Status') }}</th>
									<th> {{	__('Date') }}</th>
									<th> {{ __('Time') }}</th>
								</tr>
							</thead>
							<tbody>
								@php @endphp
								@forelse($passbook_data as $passbook_key => $passbook_value)
								@php
									$created_data = $passbook_value->created_at;
									$splitTimeStamp = explode(" ",$created_data);
									$date = date("d-m-Y", strtotime($splitTimeStamp[0]));
									$time = date("H:i:s", strtotime($splitTimeStamp[1]));
								@endphp
								<tr>
									<td>{{ $passbook_data->firstItem() + $passbook_key  }}</td>
									<td>$ {{ $passbook_value->coins }}</td>
									<td>{{ $passbook_value->status }}</td>
									<td>{{-- $passbook_value->created_at --}}{{ $date }}</td>
									<td>{{ $time }}</td>
								</tr>
								@empty
									<td colspan="4" class="text-danger">No Transection History available</td>
								@endforelse
							</tbody>
							<tfoot>
								<tr>
									<td colspan="8">{!! $passbook_data->render() !!}</td>
								</tr>
							</tfoot>
						</table>
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection