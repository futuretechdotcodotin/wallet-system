/*login form*/
$('#login-form').validate({

	rules :{
		email : {
			required : true,
			email 	 : true
		},
		password : {
			required : true
		}
	},
	messages : {
		email : {
			required : "Email address is required.",
			email 	 : "Please enter valid email address."
		},
		password : {
			required : "Password is required."
		}
	},
});

/*register form*/
$('#register-form').validate({
	rules :{
		name : {
			required : true,
			minlength 	 : 3,
			number : false,
		},
		email : {
			required : true,
			email 	 : true,
			remote 	 : {
				url: "/vkaps_wallet/wallet/check-email",
		        type: "post",
		        data: {
		          email: function() {
		            return $( "#email" ).val();
		          }
		        }
			}
		},
		password : {
			required : true,
			minlength : 6,
		},
		password_confirmation :{
			required : true,
			equalTo: "#password"
		}
	},
	messages : {
		email : {
			required : "Enter your email address.",
			email 	 : "Enter valid email address.",
			remote 	 : "Email is already used."
		},
		password : {
			required :"Enter your password",
			minlength : "Minimum 8 characters password"
		},
		password_confirmation :{
			required : "Enter your comfirm password",
			equalTo	 : "Password is not match"
		}

	}
});

/*payment form*/
$('#payment-form').validate({
	rules :{
		card_name :{
			required : true,
		},
		money : {
			required : true,
			number : true
		},
		card_number : {
			required :true,
			number : true,
			minlength : 16
		},
		cvc : {
			required : true,
			number : true,
			minlength : 3
		}
	}
})
