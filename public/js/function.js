var userId = $('#userId').val();
/*send csrf token*/
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('body').on('click','.buy', function (e) {
  e.preventDefault();
  var product_price = $(this).parent().parent().find("p.product-price").attr("price");

  swal({
    html : true,
      title:'Do you want to buy?',
      text: '',
      icon: 'warning',
      buttons: ["No", "Yes"],
  }).then(function(value) {
      if (value) {
       $.ajax({
          url:'checkPoints',
          type : "GET",
          success: function(res){
            var wallet_price =  res[0]['total_points'];
            
            if( parseInt(product_price) > parseInt(wallet_price) ){
              swal("Your wallet balance is insufficient!")
            }else{
              $.ajax({
                url : 'buy-product',
                type : "POST",
                data : {product_price:product_price},
                success: function(res){
                  if(res == "Success"){
                    swal({
                      text:   "Order successfully",
                      timer:  2000,
                      buttons: false,
                      icon: "success",
                      closeOnClickOutside: false
                    });
                    window.setTimeout(function(){
                      location.reload();
                    } ,1800);
                  }
                }
              })
            }
          }
        });
      }
  });
});