<?php

namespace App\Http\Controllers\PaymentGateway;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stripe;
use Session;
use Auth;
use App\Models\Wallet;
use App\Models\Passbook;

class StripePaymentController extends Controller
{
    /**
     * payment view
     */
    public function handleGet()
    {
        return view('payment-page');
    }
  
    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
      $user_id = $request->user_id;
      $points  = $request->money;
      $get_points = Wallet::where('user_id',$user_id)->get('total_points');
      $remaining_points = $get_points[0]->total_points;

      $total_points = (int)$remaining_points+(int)$points;

    //     /*Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));*/
        $payment_status = Stripe\Stripe::setApiKey('sk_test_51GQUdMEGSkW7kzz6xJPm4FFKzH6NwNAeNXwvnMAodNGNOKuci4F0LnV6SFdPr6FbfUVc9axQzANADYOHIKCFfynk00YSRreKoh');
        Stripe\Charge::create ([
                "amount" => 100 * (int)$points,
                "currency" => "inr",
                "source" => $request->stripeToken,
                "description" => "Making test payment." 
        ]);
  		
  		$updateWallet = Wallet::where('user_id',$user_id)->update([
          'total_points'  =>  $total_points,
      ]);

      $passbook = Passbook::create([
            'user_id' =>$user_id,
            'coins' =>$points,
            'status'=>'Credit',
        ]);

        return redirect()->route('home')->with('success','Payment has been successfully processed.');
    }
}
