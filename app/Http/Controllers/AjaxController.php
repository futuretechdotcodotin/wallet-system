<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AjaxController extends Controller
{
    public function check_email(Request $req){
    	$email = $req->email;
    	$data = User::where('email',$email)->get(['email']);
    	
    	if($data){
            return 'true';
        }else{
            return 'false';
        }
    }
}
