<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Passbook extends Model
{
    protected $fillable = [
        'user_id','status','coins',
    ];
}
