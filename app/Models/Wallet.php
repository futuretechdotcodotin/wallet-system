<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
	protected $fillable = [
        'user_id','total_points',
    ];

    public function addMoney($id)
    {
        $wallet = Wallet::create([
        	'user_id' 		=> $id,
        	'total_points' 	=> '1'
        ]);
        $wallet->save();
        return $wallet;
    }
}
