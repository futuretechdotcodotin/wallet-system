<?php
namespace App\Helpers;
use App\Models\Wallet;
use Auth;

class WalletHelper
{
	public static function getWalletPoints()
    {
    	$data =  Wallet::where('user_id',Auth::user()->id)->get('total_points')->toArray();
        return $data[0]['total_points'];/*
        return json_encode($data);*/
    }
}