<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Auth::routes();
/*ajax url*/
Route::post('/check-email','AjaxController@check_email');
Route::group(['middleware' =>['auth']], function(){
	Route::get('/add-points', function () {
	    return view('payment');
	});
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/passbook', 'Passbook\PassbookController@index')->name('view-logs');
	Route::get('/checkPoints','Wallet\WalletController@index');
	Route::post('/buy-product','Wallet\WalletController@buyProduct');
});
